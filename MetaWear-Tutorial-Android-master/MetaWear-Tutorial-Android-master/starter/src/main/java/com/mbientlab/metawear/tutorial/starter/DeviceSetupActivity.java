/*
 * Copyright 2015 MbientLab Inc. All rights reserved.
 *
 * IMPORTANT: Your use of this Software is limited to those specific rights
 * granted under the terms of a software license agreement between the user who
 * downloaded the software, his/her employer (which must be your employer) and
 * MbientLab Inc, (the "License").  You may not use this Software unless you
 * agree to abide by the terms of the License which can be found at
 * www.mbientlab.com/terms . The License limits your use, and you acknowledge,
 * that the  Software may not be modified, copied or distributed and can be used
 * solely and exclusively in conjunction with a MbientLab Inc, product.  Other
 * than for the foregoing purpose, you may not use, reproduce, copy, prepare
 * derivative works of, modify, distribute, perform, display or sell this
 * Software and/or its documentation for any purpose.
 *
 * YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
 * PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
 * NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
 * MBIENTLAB OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE,
 * STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE
 * THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED
 * TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST
 * PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY,
 * SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY
 * DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *
 * Should you have any questions regarding your right to use this Software,
 * contact MbientLab Inc, at www.mbientlab.com.
 */

package com.mbientlab.metawear.tutorial.starter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.mbientlab.metawear.ActiveDataProducer;
import com.mbientlab.metawear.DeviceInformation;
import com.mbientlab.metawear.module.Accelerometer;
import com.mbientlab.metawear.module.AccelerometerBmi160;
import com.mbientlab.metawear.module.Debug;
import com.mbientlab.metawear.module.Gpio;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.mbientlab.metawear.CodeBlock;
import com.mbientlab.metawear.Data;
import com.mbientlab.metawear.ForcedDataProducer;
import com.mbientlab.metawear.MetaWearBoard;
import com.mbientlab.metawear.Route;
import com.mbientlab.metawear.Subscriber;
import com.mbientlab.metawear.android.BtleService;
import com.mbientlab.metawear.builder.RouteBuilder;
import com.mbientlab.metawear.builder.RouteComponent;
import com.mbientlab.metawear.data.Acceleration;
import com.mbientlab.metawear.data.EulerAngles;
import com.mbientlab.metawear.module.Haptic;
import com.mbientlab.metawear.module.SensorFusionBosch;
import com.mbientlab.metawear.module.SensorFusionBosch.*;
import com.mbientlab.metawear.module.Settings;
import com.mbientlab.metawear.module.Timer;
import com.mbientlab.metawear.tutorial.starter.DeviceSetupActivityFragment.FragmentSettings;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Random;

import bolts.Continuation;
import bolts.Task;

import static android.content.DialogInterface.*;
import static java.time.LocalTime.now;

import android.graphics.Matrix;
import android.widget.ImageView;
import android.graphics.BitmapFactory;
import com.mbientlab.metawear.module.Gpio.PullMode;
import com.mbientlab.metawear.module.Gpio.PullMode;

public class    DeviceSetupActivity extends AppCompatActivity implements ServiceConnection, FragmentSettings {
    public final static String EXTRA_BT_DEVICE= "com.mbientlab.metawear.starter.DeviceSetupActivity.EXTRA_BT_DEVICE";
    private ForcedDataProducer pressureSensor;
    private ForcedDataProducer digital;
    private float P;
    private byte U;
    private byte C;
    private int W;
    private int Y;
    private Settings settings1;
    private Debug debug;
    public static class ReconnectDialogFragment extends DialogFragment implements  ServiceConnection {
        private static final String KEY_BLUETOOTH_DEVICE = "com.mbientlab.metawear.starter.DeviceSetupActivity.ReconnectDialogFragment.KEY_BLUETOOTH_DEVICE";
        private ProgressDialog reconnectDialog = null;
        private BluetoothDevice btDevice = null;
        private MetaWearBoard currentMwBoard = null;



        public static ReconnectDialogFragment newInstance(BluetoothDevice btDevice) {
            Bundle args = new Bundle();
            args.putParcelable(KEY_BLUETOOTH_DEVICE, btDevice);
            ReconnectDialogFragment newFragment = new ReconnectDialogFragment();
            newFragment.setArguments(args);

            return newFragment;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            btDevice = getArguments().getParcelable(KEY_BLUETOOTH_DEVICE);
            getActivity().getApplicationContext().bindService(new Intent(getActivity(), BtleService.class), this, BIND_AUTO_CREATE);
            reconnectDialog = new ProgressDialog(getActivity());
            reconnectDialog.setTitle(getString(R.string.title_reconnect_attempt));
            reconnectDialog.setMessage(getString(R.string.message_wait));
            reconnectDialog.setCancelable(false);
            reconnectDialog.setCanceledOnTouchOutside(false);
            reconnectDialog.setIndeterminate(true);
            reconnectDialog.setButton(BUTTON_NEGATIVE, getString(android.R.string.cancel), (dialogInterface, i) -> {
                currentMwBoard.disconnectAsync();
                getActivity().finish();
            });

            return reconnectDialog;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            currentMwBoard= ((BtleService.LocalBinder) service).getMetaWearBoard(btDevice);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) { }
    }

    private BluetoothDevice btDevice;
    private MetaWearBoard metawear;
    private final String RECONNECT_DIALOG_TAG= "reconnect_dialog_tag";
//    private Data data;
    private LineGraphSeries<DataPoint> series;

    private SensorFusionBosch sensorFusion;
    private int BPMcount=0;
    private int secondsCount=0;
    private double y;
    private double z;
    private long microsPerReading;
    private long microsPrevious;
    private long micros;
    private float[] circBufferpitch;
    private float[] circRollpitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_setup);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btDevice= getIntent().getParcelableExtra(EXTRA_BT_DEVICE);
        getApplicationContext().bindService(new Intent(this, BtleService.class), this, BIND_AUTO_CREATE);

        //Creating a graph View
        // Refer to this website for more info:http://www.android-graphview.org/


        // data

        GraphView graph = findViewById(R.id.graph);
        series = new LineGraphSeries<>();
        series.setColor(Color.RED);
        graph.addSeries(series);
        // customize a little bit viewport

        Viewport viewport = graph.getViewport();
        viewport.setYAxisBoundsManual(true);
        viewport.setMinY(0);
        viewport.setMaxY(1000);
        viewport.setXAxisBoundsManual(true);
        viewport.setMinX(0);
        viewport.setMaxX(1);

    }

        // data


    //Thread created to run the stream the graph or values
    @Override
    protected void onResume() {
        super.onResume();



        // we're going to simulate real time with thread that append data to the graph
        new Thread(new Runnable() {

            @Override
            public void run() {
                // we add 100 new entries
//                while(secondsCount<=6000) {
                while(true) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            pressureSensor.read();
                            digital.read();
//                            metawear.getModule(Haptic.class).startMotor(50.f, (short) 100);
                            series.resetData(generateData());

                            TextView Values=findViewById(R.id.Values);
                            Values.setText(""+P);
//                            if (U==1){
////                                metawear.getModule(Haptic.class).startMotor(50.f, (short) 10);
//                                U=0;
//                            }
                            TextView Tap=findViewById(R.id.touchSensor);
                            Tap.setText(""+U);

                            settings1.battery().read();
                            TextView batLevel=findViewById(R.id.batteryLevel);
                            batLevel.setText(""+W);
                            TextView motionSensor=findViewById(R.id.sensorFusion);
                            motionSensor.setText(""+Y);
                            TextView power=findViewById(R.id.powerStatus);
                            if (C==0){power.setText("Not Charging");}
                            else {power.setText("Charging");
                            }

//                                if (P>550){BPMcount+=1;}
//                                if (secondsCount==3000){
//                                    TextView Values=findViewById(R.id.Values);
//                                    Values.setText(""+BPMcount);
//                                    BPMcount=0;
//                                    secondsCount=0;
//
//                                };
//                                secondsCount++;


                        }
                    });

                    // sleep to slow down the add of entries
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // manage error ...
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {

            @Override
            public void run() {
                // we add 100 new entries
//                while(secondsCount<=6000) {
                while(true) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

//                            metawear.getModule(Haptic.class).startMotor(50.f, (short) 1000);
                        }
                    });

                    // sleep to slow down the add of entries
                    try {
                        Thread.sleep(60000);
                    } catch (InterruptedException e) {
                        // manage error ...
                    }
                }
            }
        }).start();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_device_setup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_disconnect:
                metawear.disconnectAsync();
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        metawear.disconnectAsync();
        super.onBackPressed();
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        metawear = ((BtleService.LocalBinder) service).getMetaWearBoard(btDevice);


//        gpio.pin((byte) 2).setPullMode(PullMode.PULL_UP);
        metawear.onUnexpectedDisconnect(status -> {
            ReconnectDialogFragment dialogFragment= ReconnectDialogFragment.newInstance(btDevice);
            dialogFragment.show(getSupportFragmentManager(), RECONNECT_DIALOG_TAG);

            metawear.connectAsync().continueWithTask(task -> task.isCancelled() || !task.isFaulted() ? task : MainActivity.reconnect(metawear))
                    .continueWith((Continuation<Void, Void>) task -> {
                        if (!task.isCancelled()) {
                            runOnUiThread(() -> {
                                ((DialogFragment) getSupportFragmentManager().findFragmentByTag(RECONNECT_DIALOG_TAG)).dismiss();
                                ((DeviceSetupActivityFragment) getSupportFragmentManager().findFragmentById(R.id.device_setup_fragment)).reconnected();
                            });
                        } else {
                            finish();
                        }
                        return null;
                    });
        });
        //Sensor fusion code to get the Euler angles and Linear Acceleration

        sensorFusion = metawear.getModule(SensorFusionBosch.class);

        // use ndof mode with +/-16g acc range and 2000dps gyro range
        sensorFusion.configure()
                .mode(Mode.NDOF)
                .accRange(SensorFusionBosch.AccRange.AR_16G)
                .gyroRange(GyroRange.GR_2000DPS)
                .commit();
        // stream eulerAngles values from the board
        sensorFusion.correctedAcceleration().addRouteAsync(new RouteBuilder() {
            @Override
            public void configure(RouteComponent source) {
                source.stream(new Subscriber() {
                    @Override
                    public void apply(Data data, Object... env) {
                        Y= (int)(Math.acos(data.value(CorrectedAcceleration.class).y())*180f/Math.PI);
                    }
                });
            }
        }).continueWith(new Continuation<Route, Void>() {
            @Override
            public Void then(Task<Route> task) throws Exception {
                sensorFusion.correctedAcceleration().start();
                sensorFusion.start();
                return null;
            }
        });
//        sensorFusion.eulerAngles().addRouteAsync(new RouteBuilder() {
//            @Override
//            public void configure(RouteComponent source) {
//                source.limit(50).stream(new Subscriber() {
//                    @Override
//                    public void apply(Data data, Object... env) {
////                        microsPerReading=1000000/50;
////                        microsPrevious=System.currentTimeMillis();
////
////                        for (int i=0;i<50;i++){
////                            circRollpitch[i]=data.value(CorrectedAcceleration.class).x();
////                            circBufferpitch[i]=data.value(CorrectedAcceleration.class).y();
////                        }
//                        long micros=System.currentTimeMillis();
//                        Log.i("Main Activity ","Corrected Acceleration " +data.value(EulerAngles.class).roll()+" Time: "+micros);
//                    }
//                });
//            }
//        }).continueWith(new Continuation<Route, Void>() {
//            @Override
//            public Void then(Task<Route> task) throws Exception {
//                sensorFusion.eulerAngles().start();
//                sensorFusion.start();
//                return null;
//            }
//        });
        //Replace below code
        final Gpio gpio1= metawear.getModule(Gpio.class);
        digital = gpio1.pin((byte) 6).digital();
        digital.addRouteAsync(new RouteBuilder() {
            @Override
            public void configure(RouteComponent source) {
                source.stream(new Subscriber() {
                    @Override
                    public void apply(Data data, Object ... env) {
                        //  Log.i("MainActivity", "digital state = " + data.value(Byte.class));
                        U=data.value(Byte.class);

                    }
                });
            }
        });
        //with the following code
        final Gpio gpio = metawear.getModule(Gpio.class);
        pressureSensor = gpio.pin((byte) 2).analogAdc();
        pressureSensor.addRouteAsync(new RouteBuilder() {
            @Override
            public void configure(RouteComponent source) {
                source.stream(new Subscriber() {
                    @Override
                    public void apply(Data data, Object ... env) {
                        Log.i("MainActivity", "adc = " + data.value(Short.class));
                        P=data.value(Short.class);
                    }
                });
            }
        });

        final Settings settings = metawear.getModule(Settings.class);
        settings.powerStatus().addRouteAsync(new RouteBuilder() {
            @Override
            public void configure(RouteComponent source) {
                source.stream(new Subscriber() {
                    @Override
                    public void apply(Data data, Object... env) {
                     //   Log.i("MainActivity", "power source state changed: " + data.value(Byte.class));
                        C=data.value(Byte.class);

                    }
                });
            }
        });
        settings1=metawear.getModule(Settings.class);
        settings1.battery().addRouteAsync(new RouteBuilder() {
            @Override
            public void configure(RouteComponent source) {
                source.stream(new Subscriber() {
                    @Override
                    public void apply(Data data, Object ... env) {
                        Log.i("MainActivity", "battery state = " + data.value(Settings.BatteryState.class).charge);
                        W=data.value(Settings.BatteryState.class).charge;

                    }
                });
            }
        }).continueWith(new Continuation<Route, Void>() {
            @Override
            public Void then(Task<Route> task) throws Exception {

                return null;
            }
        });



    }

    // Function created to run it in threads
    // Referred from http://www.android-graphview.org/realtime-chart/
//    private DataPoint[] generateData() {

//            int t=(int) y;
////            ImageView img = (ImageView)findViewById(R.id.imageView);
////
////            BitmapFactory.Options o = getSize(this, R.drawable.images);
////            Matrix m = new Matrix();
////            Log.i("Main Actvity","outWidth and outHeight"+o.outWidth+" " +o.outHeight);
////            m.setRotate(t, 0.5f, 0.5f);
////            img.setScaleType(ImageView.ScaleType.MATRIX);
////            img.setImageMatrix(m);
//

//    }
    private DataPoint[] generateData() {
                int count = 2;
        DataPoint[] values = new DataPoint[count];
        for (int i = 0; i <count; i++) {
            double x = i;
            DataPoint v = new DataPoint(x,P);
            values[i] = v;
        }
        return values;
    }

//    public static android.graphics.BitmapFactory.Options getSize(Context c, int resId){
//        android.graphics.BitmapFactory.Options o = new android.graphics.BitmapFactory.Options();
//        o.inJustDecodeBounds = true;
//        BitmapFactory.decodeResource(c.getResources(), resId, o);
//        return o;
//    }

    @Override
    public void onServiceDisconnected(ComponentName name) {

    }

    @Override
    public BluetoothDevice getBtDevice() {
        return btDevice;
    }
}
