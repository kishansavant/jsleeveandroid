package com.mbientlab.metawear.tutorial.Jsleevedemo;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;

import com.mbientlab.metawear.data.Acceleration;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by etsai on 11/23/16.
 */
public class CubeSurfaceView extends GLSurfaceView {
    public CubeSurfaceView(Context context) {
        super(context);
        mRenderer = new CubeRenderer();
        setRenderer(mRenderer);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    public void updateRotation(Acceleration value,double y) {
        mRenderer.acceleration = value;
        Q7=y;
        requestRender();
    }
    public void updateRotation1(Acceleration value,double z) {
        mRenderer.acceleration1= value;
        Q8=z;
        requestRender();
    }
    private class CubeRenderer implements GLSurfaceView.Renderer {
        public CubeRenderer() {
//            mCube = new Cube();
            mCube = new Square();
            mCube2=new Quad();
            mCube3=new Square1();
            mCube4=new Quad1();

        }
//////////////3dmodel///////////////
//        public void onDrawFrame(GL10 gl) {
//            gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
////            gl.glMatrixMode(GL10.GL_MODELVIEW);
//            gl.glLoadIdentity();
//            gl.glPushMatrix();
//            gl.glTranslatef(-2f, 0.5f, -18.0f);
//
//            // Convert quaterion values to glRotatef compatible values
//            // http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-17-quaternions/
//
////            float halfAngle = (float) Math.acos(quaternion.w());
////            float halfSin = (float) Math.sin(halfAngle);
////            gl.glRotatef((float) (halfAngle * 360f / Math.PI), -quaternion.x() / halfSin, -quaternion.y() / halfSin, quaternion.z() / halfSin);
//////            P1=halfAngle * 360f / (float) Math.PI;
////            gl.glRotatef(eulerAngles.heading(),-eulerAngles.roll(),-eulerAngles.pitch(),-eulerAngles.yaw());
//            P1=acceleration.y();
//            if (P1>0.85f&&P1<1.0f)
//            {
//                P2=0.07f+P1;
//            } else if(P1>-0.8&&P1<0.7)
//            {
//                P2=P1;
//            }
//            Q1=(int)(Math.acos(P2)*180f/Math.PI);
//            gl.glRotatef(Q1, 0.0f, 0.0f, 1.0f); // Rotate the square about the x-axis (NEW)
//            gl.glTranslatef(1.0f, 1.0f, 1.0f);
//            mCube.draw(gl);
//
//            gl.glPushMatrix();
//            gl.glTranslatef(2.8f, -0.5f, -1.0f); // Translate right and into the screen
//            gl.glScalef(0.7f, 0.7f, 1.0f);      // Scale down (NEW)
//
//            // Convert quaterion values to glRotatef compatible values
//            // http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-17-quaternions/
//
////            float halfAngle1 = (float) Math.acos(quaternion1.w());
////            float halfSin1 = (float) Math.sin(halfAngle1);
////            gl.glRotatef((float) (halfAngle1 * 360f / Math.PI), 0, 0, quaternion1.z() / halfSin1);
////            gl.glRotatef(angleCube, 0.0f, 0.0f, -1.0f); // rotate about the axis (1,1,1) (NEW)
////            Euler Angles formula
////            P1 =240.0f;
////            P2 = eulerAngles1.heading ();
////            if (P2 >= 0.0f && P2 <= 120.0f) {
////               Q1=P2;
////            } else if (P2 > 120.0f && P2 <= 240.0f){
////                Q1=240.0f-P2;
////            }else if (P2 > 240.0f && P2 <360.0f){
////                Q1=360.0f-P2;
////            }
////            Q2=240.0f+Q1;
////            gl.glRotatef(Q1, 0.0f, 0.0f, 1.0f); // Rotate the square about the x-axis (NEW)
//
//            P3=acceleration1.y();
//            if (P3>0.85f&&P3<1.0f)
//            {
//                P4=0.07f+P3;
//            } else if(P3>-0.8&&P3<0.7f)
//            {
//                P4=P3;
//            }
//            Q2=(int)(Math.acos(P4)*180f/Math.PI);
//            gl.glRotatef(-Q2,0.0f,0.0f,1.0f);
//            gl.glTranslatef(0.5f, 0.5f, 1.0f);
//            mCube2.draw(gl);
//            gl.glPopMatrix();
//            gl.glPopMatrix();
//        }

        ////////3dmodel////////////////


//////////////////2d arm model///////////////////////
        public void onDrawFrame(GL10 gl) {
            gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
            gl.glLoadIdentity();
            gl.glPushMatrix();
            gl.glTranslatef(-1.0f, 0f, -6.0f);
            Q5=0;
            int count=159;
            for (int i=0;i<count;i++) {
                P1 = acceleration.y()+(float)Q7;

                    if (P1>0.995f){
                                    P2=0.998f;}
                                    else P2=P1;
                Q5=Q5+P2;
            }
            Q1=Q5/count;
            Q6=180+(int)(Math.acos(Q1)*180f/Math.PI);//upper arm angle
//            Log.i("Q6","ANGLE2"+ Q6);
            gl.glRotatef(Q6, 0.0f, 0.0f, 1.0f);
            gl.glTranslatef(-0.5f, 0.5f, -6.0f);

            mCube.draw(gl);
            gl.glPushMatrix();
            gl.glTranslatef(0.7f, 5.3f, -6.0f);
            Q3=0;
            for (int i=0;i<count;i++){
                P3=acceleration1.y()+(float)Q8;
                if (P3>0.995f)
                {
                    P4=0.998f;
                }
                else {P4=P3;}
//                else if(P3>=0.92f&&P3<0.97){
//                    P4=P3-0.005f;
//                }
//                else if(P3>=0.88f&&P3<0.92){
//                    P4=P3-0.01f;
//                }
//                else if(P3>=0.84f&&P3<0.88f)
//                {
//                    P4=P3-0.03f;
//                }
//                else if(P3>=0.80f&&P3<0.84f){
//                    P4=-P3-0.05f;
//                }else if(P3>=0.76f&&P3<0.80f)
//                {
//                    P4=P3-0.07f;
//                }else if(P3>=0.72f&&P3<0.76f)
//                {
//                    P4=P3-0.09f;
//                }else if(P3>=0.68f&&P3<0.72f)
//                {
//                    P4=P3-0.11f;
//                }
//                else if(P3>=0.64f&&P3<0.68f)
//                {
//                    P4=P3-0.13f;
//                }
//                else if(P3>=0.60f&&P3<0.64f)
//                {
//                    P4=P3-0.15f;
//                }
//                else if(P3>=0.56f&&P3<0.60f)
//                {
//                    P4=P3-0.18f;
//                }
//                else if(P3>=0.52f&&P3<0.56f)
//                {
//                    P4=P3-0.21f;
//                }
//                else if(P3>=0.48f&&P3<0.52f)
//                {
//                    P4=P3-0.24f;
//                }
//                else if(P3>=0.44f&&P3<0.48f)
//                {
//                    P4=P3-0.27f;
//                }
//                else if(P3>-1.0f&&P3<0.44f)
//                {
//                    P4=P3-0.30f;
//                }
                    Q3 = Q3+P4;
            }
            Q2=Q3/count;
//            Q4=180+(int)(Math.acos(Q2)*180f/Math.PI);
//            Log.i("Q4","ANGLE1"+ Q4);
            P5=(int)(Math.acos(Q2)*180f/Math.PI)-(int)(Math.acos(Q1)*180f/Math.PI);
            if (P5<0){P5=-P5;}
//            Log.i("P5","DIFFANGLE"+P5);
            gl.glRotatef((P5), 0.0f, 0.0f, 1.0f); // Rotate the square about the x-axis (NEW)
            gl.glTranslatef(-1.0f, 1.0f,-6.0f);
            gl.glScalef(1.5f,1.5f,2f);
            mCube2.draw(gl);
            gl.glPopMatrix();
            gl.glPopMatrix();
            //New Addition for initial state of action
            gl.glTranslatef(-1.0f, 0f, -6.0f);
            gl.glRotatef(180, 0.0f, 0.0f, 1.0f);
            gl.glTranslatef(-0.5f, 0.5f, -6.0f);
        //            gl.glScalef(0.5f,0.5f,0f);
            mCube3.draw(gl);
            gl.glTranslatef(0.7f, 5.3f, -6.0f);
            gl.glRotatef(0, 0.0f, 0.0f, 1.0f); // Rotate the square about the x-axis (NEW)
            gl.glTranslatef(-1.0f, 1.0f,-6.0f);
            gl.glScalef(1.5f,1.5f,2f);
            mCube4.draw(gl);
            //

}
////////////////////////2d Arm-model///////////////////////////////////


        public void onSurfaceChanged(GL10 gl, int width, int height) {
//            gl.glViewport(0, 0, width, height);
//
//            float ratio = (float) width / height;
//            gl.glMatrixMode(GL10.GL_PROJECTION);
//            gl.glLoadIdentity();
//            gl.glFrustumf(-ratio, ratio, -1, 1, 2, 7);
            if (height == 0) height = 1;   // To prevent divide by zero
            float aspect = (float)width / height;

            // Set the viewport (display area) to cover the entire window
            gl.glViewport(0, 0, width, height);

            // Setup perspective projection, with aspect ratio matches viewport
            gl.glMatrixMode(GL10.GL_PROJECTION); // Select projection matrix
            gl.glLoadIdentity();                 // Reset projection matrix
            // Use perspective projection
            GLU.gluPerspective(gl, 45, aspect, 0.1f, 100.f);

            gl.glMatrixMode(GL10.GL_MODELVIEW);  // Select model-view matrix
            gl.glLoadIdentity();                 // Reset

            // You OpenGL|ES display re-sizing code here
            // ......
        }

        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            gl.glClearColor(0f, 0f, 0f, 1.0f);  // Set color's clear-value to black
            gl.glEnable(GL10.GL_BLEND);
            gl.glBlendFunc (GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA); //required for transparency
            gl.glClearDepthf(1.0f);            // Set depth's clear-value to farthest
            gl.glEnable(GL10.GL_DEPTH_TEST);   // Enables depth-buffer for hidden surface removal
            gl.glDepthFunc(GL10.GL_LEQUAL);    // The type of depth testing to do
            gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);  // nice perspective view
            gl.glShadeModel(GL10.GL_SMOOTH);   // Enable smooth shading of color
            gl.glDisable(GL10.GL_DITHER);      // Disable dithering for better performance
//            gl.glDisable(GL10.GL_DITHER);
//            gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT,
//                    GL10.GL_FASTEST);
//
//            gl.glClearColor(0,0,0,0);
//            gl.glEnable(GL10.GL_CULL_FACE);
//            gl.glShadeModel(GL10.GL_SMOOTH);
//            gl.glEnable(GL10.GL_DEPTH_TEST);
        }
        //        private Cube mCube;
        private Square mCube;
        private Quad mCube2;
        private Square1 mCube3;
        private Quad1 mCube4;


        public Acceleration acceleration = new Acceleration(0f,0f,0f);
        public Acceleration acceleration1 = new Acceleration(0f,0f,0f);

    }

    private CubeRenderer mRenderer;
    private float P1;
    private float P2;
    private float P3;
    private float P4;
    private float P5;
    private float Q1;
    private float Q2;
    private float Q3;
    private float Q4;
    private float Q5;
    private float Q6;
    private double Q7=0;
    private double Q8=0;
}
