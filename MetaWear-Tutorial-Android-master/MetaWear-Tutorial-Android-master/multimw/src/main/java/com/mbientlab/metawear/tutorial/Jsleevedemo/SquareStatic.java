package com.mbientlab.metawear.tutorial.Jsleevedemo;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import static javax.microedition.khronos.opengles.GL10.GL_SRC_ALPHA;


//
////////2D model///////
////
    class Square1 {
        private FloatBuffer vertexBuffer;  // Buffer for vertex-array
        private float[] vertices = {  // Vertices for the square
                //new coordinates for arm
                -0.14f,2.0f,0.0f,
                -0.38f,1.81f,0.0f,
                -0.57f,-0.71f,0.0f,
                -0.57f,-1f,0.0f,
                -0.51f,-1.32f,0.0f,
                0.56f,-1.32f,0.0f,
                0.71f,-0.91f,0.0f,
                0.73f,-0.09f,0.0f,
                0.62f,0.99f,0.0f,
                0.49f,1.64f,0.0f,
                0.44f,1.85f,0.0f,
                0.29f,2.0f,0.0f
                //
        };

        // Constructor - Setup the vertex buffer
        Square1() {
            // Setup vertex array buffer. Vertices in float. A float has 4 bytes
            ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
            vbb.order(ByteOrder.nativeOrder()); // Use native byte order
            vertexBuffer = vbb.asFloatBuffer(); // Convert from byte to float
            vertexBuffer.put(vertices);         // Copy data into buffer
            vertexBuffer.position(0);           // Rewind
        }

        // Render the shape
        public void draw(GL10 gl) {
            // Enable vertex-array and define its buffer
            gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
            gl.glEnable(GL10.GL_BLEND);//required for transparency
            gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
//            gl.glColor4f(0.81f, 0.17f, 0.16f, 0.5f);      // Jsleeve// // Set the current color (NEW)
            gl.glColor4f(0.96f, 0.96f, 0.96f, 0.3f);      //Smartherapy//
            gl.glBlendFunc (GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);//required for transparency
            // Draw the primitives from the vertex-array directly
//            gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.length / 3);
            //new addition
            gl.glLineWidth(8.5f);
            gl.glDrawArrays(GL10.GL_LINE_LOOP,0,12);
            //
            gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        }
    }

