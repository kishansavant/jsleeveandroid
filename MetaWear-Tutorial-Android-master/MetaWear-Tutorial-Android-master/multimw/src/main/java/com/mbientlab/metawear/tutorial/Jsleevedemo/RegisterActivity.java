package com.mbientlab.metawear.tutorial.Jsleevedemo;

import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mbientlab.metawear.android.BtleService;

public class RegisterActivity extends AppCompatActivity implements ServiceConnection  {
    private Intent registerintent;
    public static final int REQUEST_START_BLE_SCAN= 1;
    public static final int BLE_SCAN=2;
    public String dev_one;
    public String dev_two;
    private BtleService.LocalBinder binder_one;
    private BtleService.LocalBinder binder_two;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Context context = getApplicationContext();
        CharSequence text = "Select your boards";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
//        setSupportActionBar(findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        EditText device_one=findViewById(R.id.device_one);
		//Wes , this is where I have written the code for connecting both the devices via BLE.It calls the scanner activity which has all the parameters for BLE(strictly Mbient code-No edit)
        TextView device_one=findViewById(R.id.device_one);
        device_one.setOnClickListener(view -> startActivityForResult(new Intent(RegisterActivity.this, ScannerActivity.class), REQUEST_START_BLE_SCAN));

//        EditText device_two=findViewById(R.id.device_two);
        TextView device_two=findViewById(R.id.device_two);
        device_two.setOnClickListener(view -> startActivityForResult(new Intent(RegisterActivity.this, ScannerActivity.class),BLE_SCAN));

//        device_one.setText(dev_one);
//        device_two.setText(dev_two);

        TextView register=findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    String dev_one=device_one.getText().toString();

//                    String dev_two=device_two.getText().toString();

                    registerintent = new Intent(RegisterActivity.this, CubeActivityArm.class);
                    registerintent.putExtra("Device1",dev_one);
                    registerintent.putExtra("Device2",dev_two);
                    startActivity(registerintent);

            }
        });
        getApplicationContext().bindService(new Intent(this, BtleService.class), this, BIND_AUTO_CREATE);

    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        ///< Unbind the service when the activity is destroyed
        getApplicationContext().unbindService(this);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_START_BLE_SCAN:
                BluetoothDevice selectedDevice= data.getParcelableExtra(ScannerActivity.EXTRA_DEVICE);
//                //
//                MetaWearBoard metawear=binder_one.getMetaWearBoard(selectedDevice);
//                Led ledOne=metawear.getModule(Led.class);
//                ledOne.editPattern(Led.Color.GREEN, Led.PatternPreset.SOLID)
//                        .repeatCount((byte)5);
//                ledOne.play();
//                //
                dev_one=selectedDevice.getAddress();
                TextView device_addone=findViewById(R.id.device_one);
                device_addone.setText(dev_one);
                device_addone.setTextColor(Color.GREEN);
                break;

            case BLE_SCAN:
                BluetoothDevice selectedDevice1= data.getParcelableExtra(ScannerActivity.EXTRA_DEVICE);

//                MetaWearBoard metawear1=binder_two.getMetaWearBoard(selectedDevice1);
//                Led ledTwo=metawear1.getModule(Led.class);
//                ledTwo.editPattern(Led.Color.BLUE, Led.PatternPreset.SOLID)
//                        .repeatCount((byte)5);
//                ledTwo.play();

                dev_two=selectedDevice1.getAddress();
                TextView device_addtwo=findViewById(R.id.device_two);
                device_addtwo.setText(dev_two);
                device_addtwo.setTextColor(Color.BLUE);
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
       binder_one= (BtleService.LocalBinder) service;
       binder_two= (BtleService.LocalBinder) service;
    }
    @Override
    public void onServiceDisconnected(ComponentName name) {

    }
}
