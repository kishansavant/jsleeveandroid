package com.mbientlab.metawear.tutorial.Jsleevedemo;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.microedition.khronos.opengles.GL10;

////3d model//////
//class Quad {
//    private FloatBuffer vertexBuffer;  // Buffer for vertex-array
//    private int numFaces = 6;
//    private float[][] colors = {  // Colors of the 6 faces
//            {1.0f, 0.5f, 0.0f, 1.0f},  // 0. orange
//            {1.0f, 0.0f, 1.0f, 1.0f},  // 1. violet
//            {0.0f, 1.0f, 0.0f, 1.0f},  // 2. green
//            {0.0f, 0.0f, 1.0f, 1.0f},  // 3. blue
//            {1.0f, 0.0f, 0.0f, 1.0f},  // 4. red
//            {1.0f, 1.0f, 0.0f, 1.0f}   // 5. yellow
//    };
//    private float[] vertices = {  // Vertices of the 6 faces
//            // FRONT
//            -1.0f, -1.0f,  0.1f,  // 0. left-bottom-front
//            3.0f, -1.0f,  0.1f,  // 1. right-bottom-front
//            -1.0f,  0.5f,  0.1f,  // 2. left-top-front
//            3.0f,  0.2f,  0.1f,  // 3. right-top-front
//            // BACK
//            3.0f, -1.0f, -1.0f,  // 6. right-bottom-back
//            -1.0f, -1.0f, -1.0f,  // 4. left-bottom-back
//            3.0f,  0.2f, -1.0f,  // 7. right-top-back
//            -1.0f,  0.5f, -1.0f,  // 5. left-top-back
//            // LEFT
//            -1.0f, -1.0f, -1.0f,  // 4. left-bottom-back
//            -1.0f, -1.0f,  0.1f,  // 0. left-bottom-front
//            -1.0f,  0.5f, -1.0f,  // 5. left-top-back
//            -1.0f,  0.5f,  0.1f,  // 2. left-top-front
//            // RIGHT
//            3.0f, -1.0f,  0.1f,  // 1. right-bottom-front
//            3.0f, -1.0f, -1.0f,  // 6. right-bottom-back
//            3.0f,  0.2f,  0.1f,  // 3. right-top-front
//            3.0f,  0.2f, -1.0f,  // 7. right-top-back
//            // TOP
//            -1.0f,  0.5f,  0.1f,  // 2. left-top-front
//            3.0f,  0.2f,  0.1f,  // 3. right-top-front
//            -1.0f,  0.5f, -1.0f,  // 5. left-top-back
//            3.0f,  0.2f, -1.0f,  // 7. right-top-back
//            // BOTTOM
//            -1.0f, -1.0f, -1.0f,  // 4. left-bottom-back
//            3.0f, -1.0f, -1.0f,  // 6. right-bottom-back
//            -1.0f, -1.0f,  0.1f,  // 0. left-bottom-front
//            3.0f, -1.0f,  0.1f   // 1. right-bottom-front
//    };
//
//    // Constructor - Setup the vertex buffer
//    Quad() {
//        // Setup vertex array buffer. Vertices in float. A float has 4 bytes
//        ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
//        vbb.order(ByteOrder.nativeOrder()); // Use native byte order
//        vertexBuffer = vbb.asFloatBuffer(); // Convert from byte to float
//        vertexBuffer.put(vertices);         // Copy data into buffer
//        vertexBuffer.position(0);           // Rewind
//    }
//
//    // Render the shape
//    public void draw(GL10 gl) {
//        gl.glFrontFace(GL10.GL_CCW);    // Front face in counter-clockwise orientation
//        gl.glEnable(GL10.GL_CULL_FACE); // Enable cull face
//        gl.glCullFace(GL10.GL_BACK);    // Cull the back face (don't display)
//
//        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
//        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
//
//        // Render all the faces
//        for (int face = 0; face < numFaces; face++) {
//            // Set the color for each of the faces
//            gl.glColor4f(colors[face][0], colors[face][1], colors[face][2], colors[face][3]);
//            // Draw the primitive from the vertex-array directly
//            gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, face*4, 4);
//        }
//        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
//        gl.glDisable(GL10.GL_CULL_FACE);
//    }
//}


////////2dmodel//////
class Quad {
    private FloatBuffer vertexBuffer;  // Buffer for vertex-array
    private float[] vertices = {  // Vertices for the square

            // new coordinates for arm
            -0.29f,3.63f,0.0f,
            -0.29f,3.28f,0.0f,
            -0.19f,3.03f,0.0f,
            -0.13f,2.88f,0.0f,
            -0.12f,2.71f,0.0f,
            -0.49f,-0.74f,0.0f,
            -0.28f,-1.0f,0.0f,
            0.46f,-1.0f,0.0f,
            0.66f,-0.74f,0.0f,
            0.79f,0.16f,0.0f,
            0.79f,0.53f,0.0f,
            0.77f,0.98f,0.0f,
            0.71f,1.69f,0.0f,
            0.55f,2.71f,0.0f,
            0.62f,3.5f,0.0f,
            0.54f,3.63f,0.0f
            //
    };

    // Constructor - Setup the vertex buffer
    Quad() {
        // Setup vertex array buffer. Vertices in float. A float has 4 bytes
        ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
        vbb.order(ByteOrder.nativeOrder()); // Use native byte order
        vertexBuffer = vbb.asFloatBuffer(); // Convert from byte to float
        vertexBuffer.put(vertices);         // Copy data into buffer
        vertexBuffer.position(0);           // Rewind
    }

    // Render the shape
    public void draw(GL10 gl) {
        // Enable vertex-array and define its buffer
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
//        gl.glEnable(GL10.GL_BLEND);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
//        gl.glColor4f(0.81f, 0.17f, 0.16f, 1.0f);      //Jsleeve// // Set the current color (NEW)
        gl.glColor4f(0.96f, 0.96f, 0.96f, 1.0f);      //Smartherapy//
        // Draw the primitives from the vertex-array directly
//        gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.length / 3);
        //new addition
        gl.glLineWidth(8.5f);
        gl.glDrawArrays(GL10.GL_LINE_LOOP,0,16);
        //
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
    }
}
