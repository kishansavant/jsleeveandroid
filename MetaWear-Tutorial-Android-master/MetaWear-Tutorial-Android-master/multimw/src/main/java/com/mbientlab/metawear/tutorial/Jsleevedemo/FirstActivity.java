package com.mbientlab.metawear.tutorial.Jsleevedemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class FirstActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
//        setSupportActionBar(findViewById(R.id.toolbar));

        TextView start=findViewById(R.id.start);
        start.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent_one=new Intent(FirstActivity.this,RegisterActivity.class);
                startActivity(intent_one);
            }
            });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent_one=new Intent(FirstActivity.this,RegisterActivity.class);
                this.startActivity(intent_one);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
