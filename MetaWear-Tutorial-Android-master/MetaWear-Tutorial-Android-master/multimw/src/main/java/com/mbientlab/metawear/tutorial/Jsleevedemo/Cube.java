package com.mbientlab.metawear.tutorial.Jsleevedemo;

/**
 * Created by etsai on 11/23/16.
 */

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.microedition.khronos.opengles.GL10;

////////3d model/////////////
//
//class Cube {
//    private FloatBuffer vertexBuffer;  // Buffer for vertex-array
//    private int numFaces = 6;
//    private float[][] colors = {  // Colors of the 6 faces
//            {1.0f, 0.5f, 0.0f, 1.0f},  // 0. orange
//            {1.0f, 0.0f, 1.0f, 1.0f},  // 1. violet
//            {0.0f, 1.0f, 0.0f, 1.0f},  // 2. green
//            {0.0f, 0.0f, 1.0f, 1.0f},  // 3. blue
//            {1.0f, 0.0f, 0.0f, 1.0f},  // 4. red
//            {1.0f, 1.0f, 0.0f, 1.0f}   // 5. yellow
//    };
//    private float[] vertices = {  // Vertices of the 6 faces
//            // FRONT
//            -1.0f, -1.0f,  0.1f,  // 0. left-bottom-front
//            2.0f, -1.0f,  0.1f,  // 1. right-bottom-front
//            -1.0f,  0.5f,  0.1f,  // 2. left-top-front
//            2.0f,  0.2f,  0.1f,  // 3. right-top-front
//            // BACK
//            2.0f, -1.0f, -1.0f,  // 6. right-bottom-back
//            -1.0f, -1.0f, -1.0f,  // 4. left-bottom-back
//            2.0f,  0.2f, -1.0f,  // 7. right-top-back
//            -1.0f,  0.5f, -1.0f,  // 5. left-top-back
//            // LEFT
//            -1.0f, -1.0f, -1.0f,  // 4. left-bottom-back
//            -1.0f, -1.0f,  0.1f,  // 0. left-bottom-front
//            -1.0f,  0.5f, -1.0f,  // 5. left-top-back
//            -1.0f,  0.5f,  0.1f,  // 2. left-top-front
//            // RIGHT
//            2.0f, -1.0f,  0.1f,  // 1. right-bottom-front
//            2.0f, -1.0f, -1.0f,  // 6. right-bottom-back
//            2.0f,  0.2f,  0.1f,  // 3. right-top-front
//            2.0f,  0.2f, -1.0f,  // 7. right-top-back
//            // TOP
//            -1.0f,  0.5f,  0.1f,  // 2. left-top-front
//            2.0f,  0.2f,  0.1f,  // 3. right-top-front
//            -1.0f,  0.5f, -1.0f,  // 5. left-top-back
//            2.0f,  0.2f, -1.0f,  // 7. right-top-back
//            // BOTTOM
//            -1.0f, -1.0f, -1.0f,  // 4. left-bottom-back
//            2.0f, -1.0f, -1.0f,  // 6. right-bottom-back
//            -1.0f, -1.0f,  0.1f,  // 0. left-bottom-front
//            2.0f, -1.0f,  0.1f   // 1. right-bottom-front
//    };
//
//    // Constructor - Setup the vertex buffer
//    Cube() {
//        // Setup vertex array buffer. Vertices in float. A float has 4 bytes
//        ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
//        vbb.order(ByteOrder.nativeOrder()); // Use native byte order
//        vertexBuffer = vbb.asFloatBuffer(); // Convert from byte to float
//        vertexBuffer.put(vertices);         // Copy data into buffer
//        vertexBuffer.position(0);           // Rewind
//    }
//
//    // Render the shape
//    public void draw(GL10 gl) {
//        gl.glFrontFace(GL10.GL_CCW);    // Front face in counter-clockwise orientation
//        gl.glEnable(GL10.GL_CULL_FACE); // Enable cull face
//        gl.glCullFace(GL10.GL_BACK);    // Cull the back face (don't display)
//
//        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
//        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
//
//        // Render all the faces
//        for (int face = 0; face < numFaces; face++) {
//            // Set the color for each of the faces
//            gl.glColor4f(colors[face][0], colors[face][1], colors[face][2], colors[face][3]);
//            // Draw the primitive from the vertex-array directly
//            gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, face*4, 4);
//        }
//        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
//        gl.glDisable(GL10.GL_CULL_FACE);
//    }
//}

////////2D model///////
////
class Square {
    private FloatBuffer vertexBuffer;  // Buffer for vertex-array
    private float[] vertices = {  // Vertices for the square
            //new coordinates for arm
            -0.14f,2.0f,0.0f,
            -0.38f,1.81f,0.0f,
            -0.57f,-0.71f,0.0f,
            -0.57f,-1f,0.0f,
            -0.51f,-1.32f,0.0f,
            0.56f,-1.32f,0.0f,
            0.71f,-0.91f,0.0f,
            0.73f,-0.09f,0.0f,
            0.62f,0.99f,0.0f,
            0.49f,1.64f,0.0f,
            0.44f,1.85f,0.0f,
            0.29f,2.0f,0.0f
            //
    };

    // Constructor - Setup the vertex buffer
    Square() {
        // Setup vertex array buffer. Vertices in float. A float has 4 bytes
        ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
        vbb.order(ByteOrder.nativeOrder()); // Use native byte order
        vertexBuffer = vbb.asFloatBuffer(); // Convert from byte to float
        vertexBuffer.put(vertices);         // Copy data into buffer
        vertexBuffer.position(0);           // Rewind
    }

    // Render the shape
    public void draw(GL10 gl) {
        // Enable vertex-array and define its buffer
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
//        gl.glEnable(GL10.GL_BLEND);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
//        gl.glColor4f(0.81f, 0.17f, 0.16f, 1.0f);     // Jsleeve// // Set the current color (NEW)
        gl.glColor4f(0.96f, 0.96f, 0.96f, 1.0f);      //Smartherapy//
        // Draw the primitives from the vertex-array directly
//        gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.length / 3);
        //new addition
        gl.glLineWidth(8.5f);
        gl.glDrawArrays(GL10.GL_LINE_LOOP,0,12);
        //
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
    }
}




