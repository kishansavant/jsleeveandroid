package com.mbientlab.metawear.tutorial.Jsleevedemo;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;


    ////////2dmodel//////
    class Quad1 {
        private FloatBuffer vertexBuffer;  // Buffer for vertex-array
        private float[] vertices = {  // Vertices for the square
                //new cooridnates for arm
                -0.29f,3.63f,0.0f,
                -0.29f,3.28f,0.0f,
                -0.19f,3.03f,0.0f,
                -0.13f,2.88f,0.0f,
                -0.12f,2.71f,0.0f,
                -0.49f,-0.74f,0.0f,
                -0.28f,-1.0f,0.0f,
                0.46f,-1.0f,0.0f,
                0.66f,-0.74f,0.0f,
                0.79f,0.16f,0.0f,
                0.79f,0.53f,0.0f,
                0.77f,0.98f,0.0f,
                0.71f,1.69f,0.0f,
                0.55f,2.71f,0.0f,
                0.62f,3.5f,0.0f,
                0.54f,3.63f,0.0f
                //
        };

        // Constructor - Setup the vertex buffer
        Quad1() {
            // Setup vertex array buffer. Vertices in float. A float has 4 bytes
            ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
            vbb.order(ByteOrder.nativeOrder()); // Use native byte order
            vertexBuffer = vbb.asFloatBuffer(); // Convert from byte to float
            vertexBuffer.put(vertices);         // Copy data into buffer
            vertexBuffer.position(0);           // Rewind
        }

        // Render the shape
        public void draw(GL10 gl) {
            // Enable vertex-array and define its buffer
            gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
            gl.glEnable(GL10.GL_BLEND);         //required for transparency
            gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
//            gl.glColor4f(0.81f, 0.17f, 0.16f, 0.5f);     //Jsleeve// // Set the current color (NEW)
            gl.glColor4f(0.96f, 0.96f, 0.96f, 0.3f);      //Smartherapy//
            gl.glBlendFunc (GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA); //required for transparency
            // Draw the primitives from the vertex-array directly
//            gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.length / 3);
            //new addition
            gl.glLineWidth(8.5f);
            gl.glDrawArrays(GL10.GL_LINE_LOOP,0,16);
            //
            gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        }
    }

