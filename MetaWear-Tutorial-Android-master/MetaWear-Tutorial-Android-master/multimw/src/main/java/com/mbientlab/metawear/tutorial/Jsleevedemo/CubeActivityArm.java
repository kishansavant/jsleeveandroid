package com.mbientlab.metawear.tutorial.Jsleevedemo;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mbientlab.metawear.Data;
import com.mbientlab.metawear.ForcedDataProducer;
import com.mbientlab.metawear.MetaWearBoard;
import com.mbientlab.metawear.Route;
import com.mbientlab.metawear.Subscriber;
import com.mbientlab.metawear.android.BtleService;
import com.mbientlab.metawear.builder.RouteBuilder;
import com.mbientlab.metawear.builder.RouteComponent;
import com.mbientlab.metawear.module.Gpio;
import com.mbientlab.metawear.module.Haptic;
import com.mbientlab.metawear.module.SensorFusionBosch;
import com.mbientlab.metawear.module.SensorFusionBosch.*;

import java.text.DecimalFormat;

import bolts.Continuation;
import bolts.Task;

import static java.lang.Thread.sleep;

public class CubeActivityArm extends AppCompatActivity implements ServiceConnection {
    public static final String EXTRA_DEVICE1= "com.mbientlab.metawear.tutorial.multimw.ScannerActivity.EXTRA_DEVICE1";
    public static final String EXTRA_DEVICE2= "com.mbientlab.metawear.tutorial.multimw.ScannerActivity.EXTRA_DEVICE2";
    private ForcedDataProducer adc;
    private ForcedDataProducer touchData;
    private BluetoothDevice btDevice1;
    private MetaWearBoard board1;
    private BluetoothDevice btDevice2;
    private MetaWearBoard board2;
    private SensorFusionBosch sensorFusion1;
    private SensorFusionBosch sensorFusion2;
    private boolean sensor1flag=false;
    private boolean sensor2flag=false;
    private boolean touchflag=false;
    private BtleService.LocalBinder binder;
    private BtleService.LocalBinder binder1;
    private float Q5;
    private float Q6;
    private float Q7=0;
    private float Q8=0;
    private float Ydata1;
    private float Ydata2;
    private float Xdata1;
    private float Xdata2;
    private int Yup;
    private int Xlow;
    private int Ylow;
    private int P=0;
    private float U;
    private byte D;
    private int N=0;
    private long timeJump;
    private float[] circBuffY = new float[70];
    private float[] circBuffY1 = new float[70];
    private float[] circBuffX = new float[70];
    private float[] circBuffX1 = new float[70];
    private boolean jumpflag=false;
    private boolean upperarmflag=false;
    private boolean lowerarmflag=false;
    private boolean buttonflag=false;
    private boolean calibrateflag=false;
    private int buttonPressed=0;
    private float numOfShotsTaken=0;
    private static int DATASIZE=70;
    private int bufferCount=0;
    private int NUM_OF_TEMP_SHOTS=3;
    private int MAKE_SHOTS=0;
    private float[] shotY=new float[70];
    private float[] shotY1=new float[70];
    private float[] shotX=new float[70];
    private float[] shotX1=new float[70];
    private float accuracy=0;
    private float fieldPercentage=0;
    private float[] TemplateY_avg=new float[70];
    private float[] TemplateY1_avg=new float[70];
    private float[] TemplateX_avg=new float[70];
    private float[] TemplateX1_avg=new float[70];
    private float[] TemplateY_var=new float[70];
    private float[] TemplateY1_var=new float[70];
    private float[] TemplateX_var=new float[70];
    private float[] TemplateX1_var=new float[70];
    private int accuracycount=0;
    private float avg_accuracy=0;
    private float sumaccuracy=0;
    private ProgressBar mProgress;
    private ProgressBar mProgress2;
    private float x1;
    private float x2;
    private float y1;
    private float y2;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cubearm);
        LinearLayout l = (LinearLayout) findViewById(R.id.MyLinearLayout);//new addition//
        mGLSurfaceView = new CubeSurfaceView(this);
        l.addView(mGLSurfaceView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));//new addition//
//        setContentView(mGLSurfaceView);
        mGLSurfaceView.requestFocus();
        mGLSurfaceView.setFocusableInTouchMode(true);
        Resources res = getResources();
        Resources res1=getResources();
        Drawable drawable = res.getDrawable(R.drawable.circularfp);
        Drawable drawable1=res1.getDrawable(R.drawable.circularfp2);
        mProgress = (ProgressBar) findViewById(R.id.circularProgressbar1);
        mProgress.setProgress(0);   // Main Progress
        mProgress.setSecondaryProgress(100); // Secondary Progress
        mProgress.setMax(100); // Maximum Progress
        mProgress.setProgressDrawable(drawable);
        mProgress2 = (ProgressBar) findViewById(R.id.circularProgressbar2);
        mProgress2.setProgress(0);   // Main Progress
        mProgress2.setSecondaryProgress(100); // Secondary Progress
        mProgress2.setMax(100); // Maximum Progress
        mProgress2.setProgressDrawable(drawable1);

//        btDevice1= getIntent().getParcelableExtra(EXTRA_DEVICE1);
//        btDevice2= getIntent().getParcelableExtra(EXTRA_DEVICE2);
//        Log.i("Cube Activity","btDevice1: "+btDevice1.getAddress());
//        Log.i("Cube Activity","btDevice2: "+btDevice2.getAddress());
        //
        ///< Bind the service when the activity is created
        getApplicationContext().bindService(new Intent(this, BtleService.class), this, BIND_AUTO_CREATE);

    }

    public boolean onTouchEvent(MotionEvent touchEvent){
        switch(touchEvent.getAction()){

            case MotionEvent.ACTION_DOWN:
                x1 = touchEvent.getX();
                y1 = touchEvent.getY();
                break;
            case MotionEvent.ACTION_UP:
                x2 = touchEvent.getX();
                y2 = touchEvent.getY();
                if(x1 >x2){
                    Intent i = new Intent(CubeActivityArm.this, Screenone.class);
                    startActivity(i);
                }
                break;
        }
        return false;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        ///< Unbind the service when the activity is destroyed
        getApplicationContext().unbindService(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGLSurfaceView.onResume();


        new Thread(new Runnable() {
            @Override
            public void run() {
                // we add 100 new entries
                while(true) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            TextView Jump=findViewById(R.id.Jump);
                            //Jump algorithm
                            
                            //This is where the jumpshot is recognized
                            if ((calibrateflag)&&(sensor1flag)&&(sensor2flag)) {
                                        if (upperarmflag&&lowerarmflag&&(Math.abs(Yup - Ylow) >= 0)&&(Math.abs(Yup - Ylow)<10)) {
                                            timeJump = System.currentTimeMillis();
                                            jumpflag = true;
                                            upperarmflag=false;
                                            lowerarmflag=false;
                                            Jump.setVisibility(View.VISIBLE);
                                            Jump.postDelayed(new Runnable() {
                                                public void run() {
                                                    Jump.setVisibility(View.INVISIBLE);
                                                }
                                            }, 3000);
                                        }
                            }
                            TextView sensor_one=findViewById(R.id.upper_arm);
                            TextView sensor_two=findViewById(R.id.forearm);
                            //Below code used when external sensors are connected
                            //reading heart rate analog values from Pulse sensor
//                            try{ adc.read();}
//                            catch(NullPointerException e){}
//                            TextView Values=findViewById(R.id.hr_value);
//                            Values.setText(""+U);
//                            //reading values from touch sensor
//                            try {touchData.read();}
//                            catch(NullPointerException e){}

                            //Note:Ensure that the led on touch sensor is showing turned on
                            //if the led is not on then turn it on for testing purposes
                            //Led ON =0
                            //Led OFF=1
                            //Due to limitations of present touch sensor you have to double tap so that it resets to zero

//                            if(D==1){
//                                long timeRecord=System.currentTimeMillis();
//                                long diff=timeRecord-timeJump;
//                                if (diff<5000){
//                                    touchflag=true;
//                                }
//                            }
                            if(sensor1flag){ sensor_one.setText("ON");
                                sensor_one.setTextColor(Color.GREEN);
                            }else{
                                sensor_one.setText("OFF");

                            }
                            if(sensor2flag){
                                sensor_two.setText("ON");
                                sensor_two.setTextColor(Color.BLUE);
                            }else{sensor_two.setText("OFF");}
                        }
                    });

                    try {
                        sleep(200);
                    } catch (InterruptedException e) {
                        // manage error ...
                    }
                    // sleep to slow down the add of entries
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (buttonflag) {
                                if (buttonPressed < NUM_OF_TEMP_SHOTS + 1) {
                                    Log.i("Main Activity","ButtonSuccess");
                                    TemplateAverage();
                                }
                                buttonflag = false;
                            }
                            //this is where jumphshot is processed
                            if (bufferCount<70){
                                readIMU(bufferCount);
                                if (jumpflag) {
                                    FillBuffer();
                                    if (buttonPressed >= NUM_OF_TEMP_SHOTS)  //track shot info
                                    {
                                        DisplayAccuracy();
                                    }
                                    bufferCount = 0;
                                    jumpflag = false;
                                }
                                bufferCount++;
                                if (bufferCount==70){bufferCount=0;}
                            }

                            if((touchflag)&&(buttonPressed==NUM_OF_TEMP_SHOTS)) {
                           //Feedback motor when the touch sensor is pressed but this would work only when the boards are streaming
//                                board2.getModule(Haptic.class).startMotor(50.f, (short) 500);
                                MAKE_SHOTS++;

                            } else if((touchflag)&&(buttonPressed<NUM_OF_TEMP_SHOTS)) {
                                Context context = getApplicationContext();
                                CharSequence text = "You haven't finished with your template shots";
                                int duration = Toast.LENGTH_SHORT;
                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();
                            }
                            touchflag=false;
                            DisplayFieldPercentage();
                        }
                    });

                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // manage error ...
                    }
                }
            }
        }).start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGLSurfaceView.onPause();
    }

    private CubeSurfaceView mGLSurfaceView;

	
    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {

        binder= (BtleService.LocalBinder) service;
        binder1=(BtleService.LocalBinder) service;
        Bundle extras = getIntent().getExtras();
        String mwMacAddress1 = extras.getString("Device1");			
        String mwMacAddress2 = extras.getString("Device2");			
        
//        String mwMacAddress1= "D1:20:6F:80:C2:E0";///< Put your board's MAC address here
//        String mwMacAddress2= "CD:8B:CC:AB:D4:F4";
        BluetoothManager btManager= (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        BluetoothDevice btDevice1= btManager.getAdapter().getRemoteDevice(mwMacAddress1);
        BluetoothDevice btDevice2= btManager.getAdapter().getRemoteDevice(mwMacAddress2);
        board1 = binder.getMetaWearBoard(btDevice1);
        board2 = binder1.getMetaWearBoard(btDevice2);

        board1.onUnexpectedDisconnect(status -> runOnUiThread(() -> Log.i("Main Activity", "Disconnected")));
        board1.connectAsync().continueWithTask(task -> task.isCancelled() || !task.isFaulted() ? task : reconnect(board1)).continueWith(task -> {
            runOnUiThread(() -> {
                Log.i("Main Activity", "Connected");
                sensor1flag=true;
            });
            //Data streaming for upper arm sensor
            sensorFusion1 = board1.getModule(SensorFusionBosch.class);
            // use ndof mode with +/-16g acc range and 2000dps gyro range
            sensorFusion1.configure()
                    .mode(Mode.NDOF)
                    .accRange(SensorFusionBosch.AccRange.AR_16G)
                    .gyroRange(GyroRange.GR_2000DPS)
                    .commit();
            sensorFusion1.correctedAcceleration().addRouteAsync(source -> source.limit(70).stream((data, env) -> {
                runOnUiThread(() -> {
                    Ydata1=data.value(CorrectedAcceleration.class).y()+Q7;//new addition//
                    mGLSurfaceView.updateRotation(data.value(CorrectedAcceleration.class),Q7);
                    Yup=(int)(Math.acos(Ydata1)*180f/Math.PI);
                    if(Yup>120) {
                        upperarmflag=true;}
                    Xdata1=data.value(CorrectedAcceleration.class).x();
                });
            })).continueWith(new Continuation<Route, Void>() {
                @Override
                public Void then(Task<Route> task) throws Exception {
                    sensorFusion1.correctedAcceleration().start();
                    sensorFusion1.start();
                    return null;
                }
            });
            return null;
        });


        board2.onUnexpectedDisconnect(status -> runOnUiThread(() -> Log.i("Main Activity", "Disconnected2")));
        board2.connectAsync().continueWithTask(task -> task.isCancelled() || !task.isFaulted() ? task : reconnect(board2)).continueWith(task ->{
                    runOnUiThread(() -> {
                Log.i("Main Activity", "Connected1");
                sensor2flag=true;
            });
            //Data streaming for lower arm sensor
            sensorFusion2 = board2.getModule(SensorFusionBosch.class);
            // use ndof mode with +/-16g acc range and 2000dps gyro range
            sensorFusion2.configure()
                    .mode(Mode.NDOF)
                    .accRange(SensorFusionBosch.AccRange.AR_16G)
                    .gyroRange(GyroRange.GR_2000DPS)
                    .commit();
            sensorFusion2.correctedAcceleration().addRouteAsync(source -> source.limit(70).stream((data, env) -> {
                runOnUiThread(() -> {
                    Ydata2=data.value(CorrectedAcceleration.class).y()+Q8;   //new addition//.
                    mGLSurfaceView.updateRotation1(data.value(CorrectedAcceleration.class),Q8);

                    Ylow=(int)(Math.acos(Ydata2)*180f/Math.PI);
                    Xdata2=data.value(CorrectedAcceleration.class).x();
                    Xlow= (int)(Math.acos(Xdata2)*180f/Math.PI);
                    if ((Xlow>110)) {
                        lowerarmflag=true;}
                });
            })).continueWith(new Continuation<Route, Void>() {
                @Override
                public Void then(Task<Route> task) throws Exception {
                    sensorFusion2.correctedAcceleration().start();
                    sensorFusion2.start();
                    return null;
                }
            });
            ///Below code when external sensors are conneccted
            //Code for accessing and streaming Pulse Heart rate
//            final Gpio gpio = board2.getModule(Gpio.class);  //Getting Gpio module for Pulse Heart Rate
//            adc = gpio.pin((byte) 3).analogAdc();           //Assigning the pin to the variable adc and conversion of analog to digital values
//            adc.addRouteAsync(new RouteBuilder() {
//                @Override
//                public void configure(RouteComponent source) {
//                    source.stream(new Subscriber() {
//                        @Override
//                        public void apply(Data data, Object ... env) {
//                            U=data.value(Short.class);
//                        }
//                    });
//                }
//            });
//            //Code for accessing and streaming Touch sensor
//            final Gpio gpio1= board2.getModule(Gpio.class);
//            touchData = gpio1.pin((byte) 6).digital();
//            touchData.addRouteAsync(new RouteBuilder() {
//                @Override
//                public void configure(RouteComponent source) {
//                    source.stream(new Subscriber() {
//                        @Override
//                        public void apply(Data data, Object ... env) {
//                            D=data.value(Byte.class);
//                        }
//                    });
//                }
//            });
            return null;
        });
        //Calibration button for calibration in case of offset from straight arm posture
        Button Calibrate=(Button)findViewById(R.id.calibrate);
        Calibrate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Q5=Ydata1;
                Q6=Ydata2;
                if(Ydata1!=1.0f){
                    Q7=1-Q5;
                }
                if(Ydata2!=1.0f){
                    Q8=1-Q6;
                }
                calibrateflag=true;
            }
        });
        //Recording the template shot
        Button Record=findViewById(R.id.templateshot);
        Record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long timeRecord=System.currentTimeMillis();
                long diff=timeRecord-timeJump;
                if ((diff)<3000){
                    Log.i("Main Activity","diff"+diff);
                buttonflag=true;
                buttonPressed++;
                N=N+1;
                }
                TextView shotsTaken=findViewById(R.id.shotsTaken);
                shotsTaken.setText(""+N);
            }
        });
        TextView MakeShot=findViewById(R.id.madeShot);
        MakeShot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long timeRecord=System.currentTimeMillis();
                long diff=timeRecord-timeJump;
                if (diff<5000){
                    touchflag=true;
                }
            }
        });

    }

    private void readIMU(int count){
        circBuffY[count]=Ydata1;
        circBuffY1[count]=Ydata2;
        circBuffX[count]=Xdata1;
        circBuffX1[count]=Xdata2;
    }
    private void FillBuffer(){
        float YOffset = 0;
        float Y1Offset = 0;
        float XOffset = 0;
        float XOffset1=0;
        int i;
        for(i=0; i<DATASIZE; i++)
        {
            shotY[i] = circBuffY[i];
            shotY1[i]=circBuffY1[i];
            shotX[i]=circBuffX[i];
            shotX1[i]=circBuffX1[i];
            YOffset += shotY[i];
            Y1Offset += shotY1[i];
            XOffset += shotX[i];
            XOffset1+=shotX1[i];

        }

        /*Normalize data to zero*/
        // offset is average of data set
        YOffset = YOffset/DATASIZE;
        Y1Offset = Y1Offset/DATASIZE;
        XOffset = XOffset/DATASIZE;
        XOffset1=XOffset1/DATASIZE;

        for(i=0; i<DATASIZE; i++)
        {
            shotY[i] -= YOffset;
            shotY1[i] -= Y1Offset;
            shotX[i]-=XOffset;
            shotX1[i]-=XOffset1;
        }
    }
    public void TemplateAverage()
    {
        for(int i=0;i<DATASIZE;i++)
        {
            //calculate average
           TemplateY_avg[i]+= shotY[i]/NUM_OF_TEMP_SHOTS;
           TemplateY1_avg[i]+= shotY1[i]/NUM_OF_TEMP_SHOTS;
           TemplateX_avg[i]+= shotX[i]/NUM_OF_TEMP_SHOTS;
           TemplateX1_avg[i]+=shotX1[i]/NUM_OF_TEMP_SHOTS;

//            calculate variance
            TemplateY_var[i] += (float)(Math.pow(shotY[i],2)/NUM_OF_TEMP_SHOTS);
            TemplateY1_var[i] += (float)(Math.pow(shotY1[i],2)/NUM_OF_TEMP_SHOTS);
            TemplateX_var[i]+= (float)(Math.pow(shotX[i],2)/NUM_OF_TEMP_SHOTS);
            TemplateX1_var[i]+= (float)(Math.pow(shotX1[i],2)/NUM_OF_TEMP_SHOTS);
        }
    }

    //
    private float CalcAccuracy()
    {
        float deviation;
        float maxDev;
        float accuracy1;
        float accuracyY=0, accuracyY1=0;
        float accuracyX=0, accuracyX1=0;

        for(int i = 0; i<DATASIZE; i++)
        {
            //accuracy for pitch data up
            deviation = Math.abs(TemplateY_avg[i]-shotY[i]); //calculates devation from mean (average)
            maxDev = (float)(3*Math.sqrt(TemplateY_var[i])); //square root of variance gives standard deviation

            //make deviation equal the max deviation (3 x standard dev) if deviation greater than maxDev
            if(deviation > maxDev)
                deviation = maxDev;
            accuracyY += 1-(deviation/maxDev); //accuracy is higher when deviation is smaller
            //accuracy for pitch data down
            deviation = Math.abs(TemplateY1_avg[i]-shotY1[i]); //calcuulates devation from mean (average)
            maxDev = (float)(3*Math.sqrt(TemplateY1_var[i])); //square root of variance gives standard deviation
            //make deviation equal the max deviation (3 x standard dev) if deviation greater than maxDev
            if(deviation > maxDev)
                deviation = maxDev;
            accuracyY1 += 1-(deviation/maxDev); //accuracy is higher when deviation is smaller

            //accuracy for roll data up
            deviation = Math.abs(TemplateX_avg[i]-shotX[i]); //calcuulates devation from mean (average)
            maxDev = (float)(3*Math.sqrt(TemplateX_var[i])); //square root of variance gives standard deviation

            //make deviation equal the max deviation (3 x standard dev) if deviation greater than maxDev
            if(deviation > maxDev)
                deviation = maxDev;
            accuracyX += 1-(deviation/maxDev); //accuracy is higher when deviation is smaller

            //accuracy for roll data down
            deviation = Math.abs(TemplateX1_avg[i]-shotX1[i]); //calcuulates devation from mean (average)
            maxDev = (float)(3*Math.sqrt(TemplateX1_var[i])); //square root of variance gives standard deviation

            //make deviation equal the max deviation (3 x standard dev) if deviation greater than maxDev
            if(deviation > maxDev)
                deviation = maxDev;
            accuracyX1 += 1-(deviation/maxDev); //accuracy is higher when deviation is smaller
        }
        accuracy1 = (float)(((accuracyY/DATASIZE)*0.3 + (accuracyY1/DATASIZE)*0.3+(accuracyX/DATASIZE)*0.2+(accuracyX1/DATASIZE)*0.2 )*100); //40% weighted towards roll and 60% weighted towards pitch

        return accuracy1;
    }
    private float getFieldPercentage()
    {
        TextView fG=findViewById(R.id.fieldgoal);
        fG.setText(""+MAKE_SHOTS);
        //don't count shots or button presses before template creation
        fieldPercentage = ((MAKE_SHOTS/(numOfShotsTaken))*100);
        if (MAKE_SHOTS==0){fieldPercentage=0;}
        return fieldPercentage;
    }

    public void DisplayAccuracy(){
        accuracycount=accuracycount+1;
        accuracy = CalcAccuracy();
        sumaccuracy=sumaccuracy+accuracy;
        avg_accuracy=sumaccuracy/accuracycount;
        DecimalFormat f = new DecimalFormat("##.00");
        TextView avgAccuracy=findViewById(R.id.avgaccuracy);
        avgAccuracy.setText(""+f.format(avg_accuracy));
             //code for restricting it to two decimals
        TextView Accuracy=findViewById(R.id.accuracy);
        mProgress.setProgress((int)accuracy);
        Accuracy.setText(""+f.format(accuracy));
        if (accuracy>30.00){
            numOfShotsTaken++;
            P = P + 1;
            TextView Count = findViewById(R.id.Count);
            Count.setText("" + P);}
    }
    public void DisplayFieldPercentage(){

        fieldPercentage = getFieldPercentage();
        DecimalFormat f = new DecimalFormat("##.00");
        TextView FieldPercent=findViewById(R.id.fieldpercentage);
        mProgress2.setProgress((int)fieldPercentage);
        FieldPercent.setText(""+f.format(fieldPercentage));

    }

    public static Task<Void> reconnect(final MetaWearBoard board) {
        return board.connectAsync().continueWithTask(task -> task.isFaulted() ? reconnect(board) : task);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
    }
}
